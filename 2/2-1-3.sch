;;;; 2.1.3

(define (cons* x y)
  (define (dispatch m)
    (cond ((= m 0) x)
          ((= m 1) y)
          (else (error "Arugment not 0 || 1 -- cons" m))))
  dispatch)

(define (car* z) (z 0))

(define (cdr* z) (z 1))


;; ex 2.4

(define (cons1 x y)
  (lambda (m) (m x y)))

(define (car1 z)
  (z (lambda (p q) p)))

(define (cdr1 z)
  (z (lambda (p q) q)))

(define *cons-1-1* (cons1 2 3))

(car1 *cons-1-1*)
(cdr1 *cons-1-1*)


;; ex 2.5


